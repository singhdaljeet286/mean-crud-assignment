# Backend setup
npm init -y
npm i --save express mongoose
npm i cors body-parser dotenv multer

# Frontend setup & build
ng new frontend
npm run build

# running
npm start
You can access the web app at http://localhost:3000
